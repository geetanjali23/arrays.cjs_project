// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
function filter(elements, cb) {
    if (typeof elements != "object" || typeof cb != "function") {
        return [];
    }
    let resArr = [];
    for(let value of elements) {
        if(cb(value)) {
            resArr.push(value);
        }
    }
    return resArr;
}
module.exports = filter;
