// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.
function find(elements, cb) {
    if (!(Array.isArray(elements)) || typeof cb != "function") {
        return [];
    }
    for (let value of elements) {
        if (cb(value)) {
            return value;
        }
    }
    return undefined;
}
module.exports = find;