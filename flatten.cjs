// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

let flattenAnswerArr = [];
function flatten(givenArr, depth = 1) {
    if (!Array.isArray(givenArr) || givenArr.length === 0) {
        return [];
    }
    for (let index = 0; index < givenArr.length; index++) {
        if (Array.isArray(givenArr[index])) {
            if (depth >= 1) {
                flatten(givenArr[index], depth - 1);
            } else {
                flattenAnswerArr.push(givenArr[index]);
            }
        } else {
            if (givenArr[index] != undefined) {
                flattenAnswerArr.push(givenArr[index]);
            }
        }
    }
    return flattenAnswerArr;
}
module.exports = flatten;