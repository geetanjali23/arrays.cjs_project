// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and function returns a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.
function map(elements, cb) {
    if (typeof elements != "object" || typeof cb != "function") {
        return [];
    }
    let newArr = [];
    for (let idx = 0; idx < elements.length; idx++) {
        newArr.push(cb(elements[idx], idx));
    }
    return newArr;
}
module.exports = map;
