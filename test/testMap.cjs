const obj = require("../data/arrays.cjs");
const map = require("../map.cjs");
function cb(item, index) {
    return item * 2 + index;
}
console.log(map(obj.items, cb));